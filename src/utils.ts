export async function getEl(selector: string): Promise<HTMLElement> {
    return await document.querySelector(selector) as HTMLElement;
};

export async function getEls(selector: string): Promise<Array<Element>> {
    return await Array.from(document.querySelectorAll(selector));
}

export async function createEl(tag: string): Promise<HTMLElement> {
    return await document.createElement(tag)
}

export async function getAttr(el: HTMLElement, attr: string) {
    return await el.getAttribute(attr);
}

export async function addListener(el: HTMLElement, eventName: string, fn: EventListenerOrEventListenerObject) {
    return await el.addEventListener(eventName, fn)
}